import utfpr.ct.dainf.if62c.projeto.PoligonalFechada;
import utfpr.ct.dainf.if62c.projeto.PontoXZ;

public class Projeto1 {

  public static void main(String[] args) {
    PontoXZ[] pontos = new PontoXZ[3];
    
    pontos[0] = new PontoXZ(-3, 2);
    pontos[1] = new PontoXZ(-3, 6);
    pontos[2] = new PontoXZ(0, 2); 
    
    PoligonalFechada pf = new PoligonalFechada(pontos);  
    
    System.out.println(String.format("Comprimento da poligonal = %.3f", pf.getComprimento()));
  }
    
}