package utfpr.ct.dainf.if62c.projeto;

public class PontoXZ extends Ponto2D {

  public PontoXZ() {
    super();
  }

  public PontoXZ(double x, double z) {
    super(x, 0, z);
  }

  @Override
  public String toString() {
    return(String.format("%s(%f,%f)", this.getNome(), this.getX(), this.getZ()));
  } 
    
}