package utfpr.ct.dainf.if62c.projeto;

public class PontoYZ extends Ponto2D {

  public PontoYZ() {
    super();
  }

  public PontoYZ(double y, double z) {
    super(0, y, z);
  }

  @Override
  public String toString() {
    return(String.format("%s(%f,%f)", this.getNome(), this.getY(), this.getZ()));
  }
        
}