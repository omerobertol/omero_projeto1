package utfpr.ct.dainf.if62c.projeto;

public class PontoXY extends Ponto2D {

  public PontoXY() {
    super();  
  }

  public PontoXY(double x, double y) {
    super(x, y, 0);
  }

  @Override
  public String toString() {
    return(String.format("%s(%f,%f)", this.getNome(), this.getX(), this.getY()));
  }
    
}