package utfpr.ct.dainf.if62c.projeto;

public class Ponto {
  private double x, y, z;

  public Ponto() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }

  public Ponto(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public double getX() {
    return x;
  }

  public void setX(double x) {
    this.x = x;
  }

  public double getY() {
    return y;
  }

  public void setY(double y) {
    this.y = y;
  }

  public double getZ() {
    return z;
  }

  public void setZ(double z) {
    this.z = z;
  }
    
  public double dist(Ponto p) {
    double d;
    d = Math.sqrt(Math.pow((this.x - p.x), 2) + Math.pow((this.y - p.y), 2) + Math.pow((this.z - p.z), 2));
    return(d); 
  } 

  public String getNome() {
    return getClass().getSimpleName();
  }

  @Override
  public String toString() {
    return(String.format("%s(%f,%f,%f)", getNome(), x, y, z));
  }

  @Override
  public boolean equals(Object obj) {
    try {   
      if ((obj instanceof Ponto) && (this.x == ((Ponto)obj).x) && (this.y == ((Ponto)obj).y) && ((this.z == ((Ponto)obj).z)))
         return(true);
      else return(false);
    } catch (Exception e) {
        return(false);
    } 
  }

}