package utfpr.ct.dainf.if62c.projeto;

public class PoligonalFechada<T extends Ponto2D> extends Poligonal {

  public PoligonalFechada(T[] vertices) {
    super(vertices);
  }
  
  @Override
  public double getComprimento() {
    int i;
    double comprimento = 0;
    Ponto p;
    
    p = this.get(0);
    for (i=1; i<this.getN(); i++) {
      comprimento += p.dist(this.get(i));
      
      p = this.get(i);
    }
    comprimento += p.dist(this.get(0)); // fecha a poligonal: o último ponto com o primeiro
    
    return(comprimento);  
  }
      
}