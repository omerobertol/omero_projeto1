package utfpr.ct.dainf.if62c.projeto;

public abstract class Ponto2D extends Ponto {

    protected Ponto2D() {
      super();  
    }

    protected Ponto2D(double x, double y, double z) {
      super(x, y, z);
    }

}