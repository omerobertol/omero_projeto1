package utfpr.ct.dainf.if62c.projeto;

public class Poligonal<T extends Ponto2D> {
  private T[] vertices;

  public Poligonal(T[] vertices) {
    if ((vertices == null) || (vertices.length < 2))
       throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
    
    this.vertices = vertices;
  }
  
  public int getN() {
    return(this.vertices.length);  
  }

  public T get(int i) {
    if ((i < 0) || (i >= this.getN()))
       return(null);
    else return(this.vertices[i]);
  }
  
  public void set(int i, T vertice) {
    if ((i >= 0) && (i < this.getN()))
       this.vertices[i] = vertice;     
  }
  
  public double getComprimento() {
    int i;
    double comprimento = 0;
    Ponto p;
    
    p = this.get(0);
    for (i=1; i<this.getN(); i++) {
      comprimento += p.dist(this.get(i));
      
      p = this.get(i);
    }
    
    return(comprimento);  
  }
  
}